#include "prints.hpp"
#include <iostream>
#include <sstream>

#include "compiler.hpp"
#include "version.hpp"

void PrintMatchedOnly::print(T_STRING& match,T_STRING& result){
  //T_COUT << match << C_CONST("\n");
  std::cout << SYS_TO_MY_TYPE(match) << "\n";
}

void PrintResultOnly::print(T_STRING& match,T_STRING& result){
  //T_COUT << result << C_CONST("\n");
  std::cout << SYS_TO_MY_TYPE(result) << "\n";
}

void PrintMatchAndResult::print(T_STRING& match,T_STRING& result){
  //T_COUT << match << C_CONST("\t--> ") << result << C_CONST("\n");
  std::stringstream SS("");
  SS << SYS_TO_MY_TYPE(match) << " --> " << SYS_TO_MY_TYPE(result);
  std::cout << SS.str() << "\n";
}

void printRenameError(const int e){
  switch(e){
    default:
      std::cerr << "Unknown error while renaming file or folder. #" << e << std::endl; break;
    case EACCES:
      std::cerr << "One of the files or directories refuses write permission, or already exists or can not be created." << std::endl; break;
    case ENOENT:
      std::cerr << "File or folder not found." << std::endl; break;
    case EINVAL:
      std::cerr << "Name contains invalid characters." << std::endl; break;
    case EBUSY:
      std::cerr << "File or folder is being used by the system." << std::endl; break;
    case ENOTEMPTY:
    case EEXIST:
      std::cerr << "Target name already exists and are not empty." << std::endl; break;
    case EISDIR:
      std::cerr << "New name is directory but old name is not." << std::endl; break;
  };
}

void printHeader(){
  std::cout << "DIG-Rename " << DIGRENAME_VERSION << " :. File and folder renamer with pattern and regex support.\n" << std::endl;
}

void printUsage(const T_CHAR* a0){
  T_COUT << C_CONST(" Usage:   ") << a0 << C_CONST(" [OPTIONS] pattern-match pattern-replace") << std::endl;
}

void printVersion(){
  std::cout << "DIG-Rename " << DIGRENAME_VERSION << std::endl;
  std::cout << DIGRENAME_COPYRIGHT << std::endl;
  #ifdef I_WINDOWS
  std::cout << "Built with " << COMPILER_NAME << " (" << PLATFORM << ") with Unicode support." << std::endl;
  #else
  std::cout << "Built with " << COMPILER_NAME << " (" << PLATFORM << ")" << std::endl;
  #endif
}

void printManual(const T_CHAR* a0){
  printHeader();
  printUsage(a0);
  std::cout << " Example: drename \"|*.old\" \"|*.new\"" << std::endl;
  std::cout << "   It will rename all files terminated with .old to .new.\n\n";
  std::cout << " Pattern:\n   You can use * or ? like command line usage to reffer to multiple files.\n   Sometimes you will need to escape this symbols, like in example.\n   You can do that with \\ ou |, the | always will work.\n";
  std::cout << "   You can use regex too.\n   In this case use -R or --regex-only to not parse the patterns.\n   Remember to use backreference (aka. $1, $2, ...) to reffer the extracted group.\n";
  std::cout << "   Use --preview while testing your pattern, it will not rename files, but will show all matched files." << std::endl;
}