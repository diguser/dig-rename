#include "regex_std.hpp"
#include <iostream>


inline void StringReplaceAll(T_STRING& str,const T_STRING find,const T_STRING replace){
  auto pos = str.find(find);
  while(pos != std::string::npos){
    str.replace(pos,find.length(),replace);
    pos = str.find(find,pos+replace.length());
  }
}

/**
 * Defaults Regex functions
 */
void Regex::internalParseInput(T_STRING& str){
  auto pos = str.find(C_CONST("*"));
  #define ANY_CHARACTER "[[:graph:] ]"
  #define ANY_THING ".*"
  if(pos != std::string::npos || str.find(C_CONST("?")) != std::string::npos){
    StringReplaceAll(str,C_CONST("("),C_CONST("\\("));
    StringReplaceAll(str,C_CONST(")"),C_CONST("\\)"));
    StringReplaceAll(str,C_CONST("["),C_CONST("\\["));
  }
  while((pos = str.find(C_CONST("\\*"))) != std::string::npos){
    str.replace(pos,2,C_CONST("(" ANY_THING ")"));
  }
  while((pos = str.find(C_CONST("|*"))) != std::string::npos){
    str.replace(pos,2,C_CONST("(" ANY_THING ")"));
  }
  pos = 0;
  while((pos = str.find(C_CONST("*"),pos)) != std::string::npos){
    if((pos != 0)&&(pos-1 == str.find(C_CONST(".*"),pos-1))){
      pos++;
      continue;
    }
    str.replace(pos,1,C_CONST("(" ANY_THING ")"));
    pos++;
  }
  while((pos = str.find(C_CONST("\\?"))) != std::string::npos){
    str.replace(pos,2,C_CONST("(" ANY_CHARACTER ")"));
  }
  while((pos = str.find(C_CONST("|?"))) != std::string::npos){
    str.replace(pos,2,C_CONST("(" ANY_CHARACTER ")"));
  }
  while((pos = str.find(C_CONST("?"))) != std::string::npos){
    str.replace(pos,1,C_CONST("(" ANY_CHARACTER ")"));
  }
}

void Regex::internalParseOutput(T_STRING& str){
  auto pos  = str.find(C_CONST("\\*"));
  auto pos1 = str.find(C_CONST("|*"));
  auto pos2 = str.find(C_CONST("*"));
  auto pos3 = str.find(C_CONST("\\?"));
  auto pos4 = str.find(C_CONST("|?"));
  auto pos5 = str.find(C_CONST("?"));
  auto pos6 = str.find(C_CONST("?"));
  for(
    int count = 1;
    (pos !=std::string::npos)||(pos1!=std::string::npos)||(pos2!=std::string::npos)||
    (pos3!=std::string::npos)||(pos4!=std::string::npos)||(pos5!=std::string::npos);
    pos =str.find(C_CONST("\\*")),
    pos1=str.find(C_CONST("|*")),
    pos2=str.find(C_CONST("*")),
    pos3=str.find(C_CONST("\\?")),
    pos4=str.find(C_CONST("|?")),
    pos5=str.find(C_CONST("?")),
    count++
  ){
  /*
  while(((pos =str.find(C_CONST("\\*")))!=std::string::npos)||
        ((pos1=str.find(C_CONST("|*")))!=std::string::npos)||
        ((pos2=str.find(C_CONST("*")))!=std::string::npos)||
        ((pos3=str.find(C_CONST("\\?")))!=std::string::npos)||
        ((pos4=str.find(C_CONST("|?")))!=std::string::npos)||
        ((pos5=str.find(C_CONST("?")))!=std::string::npos)){
    count++;
  */
    pos6 = std::min({pos,pos1,pos2,pos3,pos4,pos5});
    if((pos6 == pos2)||(pos6 == pos5)){
      str.replace(pos6,1,T_STRING(C_CONST("$"))+F_TO_STRING(count));
    }else{
      str.replace(pos6,2,T_STRING(C_CONST("$"))+F_TO_STRING(count));
    }
  }
}

/**
 * Basic functions of std::regex
 */
RegexStd::_RegexStd():error(std::regex_constants::error_collate){}

RegexStd::~RegexStd(){}

bool RegexStd::match(T_STRING& str){
  return std::regex_match(str,regex);
}

bool RegexStd::replace(T_STRING& in,T_STRING& pattern,T_STRING& out){
  out = regex_replace(in,regex,pattern);
  return true;
}

bool RegexStd::hasError(){
  return hError;
}

void RegexStd::printError(){
  switch(error.code()){
    case std::regex_constants::error_collate:
      std::cerr << "The expression contained an invalid collating element name." << std::endl; break;
    case std::regex_constants::error_ctype:
      std::cerr << "The expression contained an invalid character class name." << std::endl; break;
    case std::regex_constants::error_escape:
      std::cerr << "The expression contained an invalid escaped character, or a trailing escape." << std::endl; break;
    case std::regex_constants::error_backref:
      std::cerr << "The expression contained an invalid back reference." << std::endl; break;
    case std::regex_constants::error_brack:
      std::cerr << "The expression contained mismatched brackets ([ and ])." << std::endl; break;
    case std::regex_constants::error_paren:
      std::cerr << "The expression contained mismatched parentheses (( and ))." << std::endl; break;
    case std::regex_constants::error_brace:
      std::cerr << "The expression contained mismatched braces ({ and })." << std::endl; break;
    case std::regex_constants::error_badbrace:
      std::cerr << "The expression contained an invalid range between braces ({ and })." << std::endl; break;
    case std::regex_constants::error_range:
      std::cerr << "The expression contained an invalid character range." << std::endl; break;
    case std::regex_constants::error_space:
      std::cerr << "There was insufficient memory to convert the expression into a finite state machine." << std::endl; break;
    case std::regex_constants::error_badrepeat:
      std::cerr << "The expression contained a repeat specifier (one of *?+{) that was not preceded by a valid regular expression." << std::endl; break;
    case std::regex_constants::error_complexity:
      std::cerr << "The complexity of an attempted match against a regular expression exceeded a pre-set level." << std::endl; break;
    case std::regex_constants::error_stack:
      std::cerr << "There was insufficient memory to determine whether the regular expression could match the specified character sequence." << std::endl; break;
    default:
      std::cerr << "An unknown error occour while interpreting regex input." << std::endl; break;
  }
}

/**
 * Specialized std::regex initializers
 */
#define COMMON_REGEX_FLAGS std::regex::optimize
bool RegexStdExtendedPosix::parseRegex(T_STRING& pattern){
  try{
    regex = iRegex(pattern,COMMON_REGEX_FLAGS|std::regex::extended);
  }catch(std::regex_error& e){
    error = e;
    hError = true;
    return false;
  }catch(...){
    std::cerr << "Unknown exception with std::regex." << std::endl;
    hError = true;
    return false;
  }
  return true;
}

#ifndef NOT_STANDART_BASIC_POSIX
bool RegexStdBasicPosix::parseRegex(T_STRING& pattern){
  try{
    regex = iRegex(pattern,COMMON_REGEX_FLAGS|std::regex::basic);
  }catch(std::regex_error& e){
    error = e;
    hError = true;
    return false;
  }catch(...){
    std::cerr << "Unknown exception with std::regex." << std::endl;
    hError = true;
    return false;
  }
  return true;
}
#endif

#ifndef NOT_STANDART_EGREP
bool RegexStdEgrep::parseRegex(T_STRING& pattern){
  try{
    regex = iRegex(pattern,COMMON_REGEX_FLAGS|std::regex::egrep);
  }catch(std::regex_error& e){
    error = e;
    hError = true;
    return false;
  }catch(...){
    std::cerr << "Unknown exception with std::regex." << std::endl;
    hError = true;
    return false;
  }
  return true;
}
#endif

#ifndef NOT_STANDART_GREP
bool RegexStdGrep::parseRegex(T_STRING& pattern){
  try{
    regex = iRegex(pattern,COMMON_REGEX_FLAGS|std::regex::grep);
  }catch(std::regex_error& e){
    error = e;
    hError = true;
    return false;
  }catch(...){
    std::cerr << "Unknown exception with std::regex." << std::endl;
    hError = true;
    return false;
  }
  return true;
}
#endif
