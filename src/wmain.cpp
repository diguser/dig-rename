#include "unicode.hpp"

#ifdef I_WINDOWS

#include <windows.h>    // GetCommandLine, CommandLineToArgvW, LocalFree
#include <stdlib.h>     // EXIT_FAILURE
#include <iostream>

extern int wmain(int,wchar_t**);

int main(){
    struct Args {
      int n;
      wchar_t** p;
      ~Args() {  if(p != nullptr){::LocalFree(p);}}
      Args(): p(::CommandLineToArgvW(::GetCommandLineW(),&n)){}
    };

    Args args;

    if(args.p == nullptr){
      return EXIT_FAILURE;
    }
    SetConsoleOutputCP(CP_UTF8);
    return wmain(args.n,args.p);
}

#endif