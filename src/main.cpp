#include <algorithm>
#include <dirent.h>
#include <iostream>
#include <regex>
#include <string>
#include <cxxopts.hpp>

#include "prints.hpp"
#include "unicode.hpp"
#include "regex.hpp"
#include "regex_std.hpp"

#ifdef I_WINDOWS
  #include <windows.h>
#endif

#define MY_FREE(a) if(a != nullptr) delete a;

int F_MAIN(int ac,T_CHAR** av){  
  bool preview_only = false;
  bool debug = false;
  Regex* principal=nullptr;
  T_STRING output;
  PrintMatchedFile* print_matched_file = nullptr;
  {
    bool exclusive_regex = false;
    #ifndef NOT_STANDART_BASIC_POSIX
    bool regex_basic_posix = false;
    #endif
    #ifndef NOT_STANDART_GREP
    bool regex_grep = false;
    #endif
    #ifndef NOT_STANDART_EGREP
    bool regex_egrep = false;
    #endif
    bool regex_extend_posix = false;
    
    if((ac <= 2)||(ac >= 4)){
      bool verbose = false;
      bool view_match = false;
      bool view_result = false;
      
      char** av_ = new char*[ac];
      {
        std::string tmp;
        for(int i = 0; i<ac; i++){
          tmp = SYS_TO_MY_TYPE(av[i]);
          av_[i] = new char[tmp.length()+1];
          av_[i][tmp.length()] = '\0';
          memcpy(av_[i],tmp.c_str(),tmp.length());
        }
      }
      
      cxxopts::Options opts(av_[0],"File renamer with regex support.");
      opts.add_options()
        ("h,help","Show this help.")
        ("man","Show a brief manual with usage instructions.")
        ("p,preview","Like 'verbose', but does not rename any file or folder.",cxxopts::value<bool>(preview_only))
        ("view-match","Show every file or folder who match the regex.",cxxopts::value<bool>(view_match))
        ("view-result","Show every result of file or folder matched.",cxxopts::value<bool>(view_result))
        ("verbose","Show every file or folder who match and the result.",cxxopts::value<bool>(verbose))
        ("v,version","Show version.");
      opts.add_options("Regex")
        ("R,regex-only","Assume input and output as pure regex.",cxxopts::value<bool>(exclusive_regex))
        #ifndef NOT_STANDART_BASIC_POSIX
        ("b,basic-posix","Use basic POSIX grammar in regex.",cxxopts::value<bool>(regex_basic_posix))
        #endif
        ("extended-posix","Use extended POSIX grammar in regex. (default)",cxxopts::value<bool>(regex_extend_posix))
        #ifndef NOT_STANDART_EGREP
        ("e,egrep","Use egrep grammar in regex.",cxxopts::value<bool>(regex_egrep))
        #endif
        #ifndef NOT_STANDART_GREP
        ("g,grep","Use grep grammar in regex.",cxxopts::value<bool>(regex_grep))
        #endif
        ;
      opts.add_options("debug")
        ("debug","--",cxxopts::value<bool>(debug));
      int ac_ = (ac>=4?ac-2:ac);
      opts.parse(ac_,av_);
      for(int i=0;i<ac;i++){
        delete av_[i];
      }
      delete av_;
      
      if((opts.count("help"))||(ac == 1)){
        std::string hlp = opts.help({"","Regex"});
        hlp = hlp.substr(hlp.find("\n\n")+2);
        printHeader();
        printUsage(av[0]);
        std::cout << " Options:\n" << hlp << std::endl;
        return 0;
      }
      if(opts.count("version")){
        printVersion();
        return 0;
      }
      if(opts.count("man")){
        printManual(av[0]);
        return 0;
      }
      if(preview_only||verbose||(view_match&&view_result)){
        print_matched_file = new PrintMatchAndResult();
      }else if(view_match){
        print_matched_file = new PrintMatchedOnly();
      }else if(view_result){
        print_matched_file = new PrintResultOnly();
      }
    }
    
    if(regex_extend_posix){
      principal = new RegexStdExtendedPosix();
    }else
    #ifndef NOT_STANDART_BASIC_POSIX
    if(regex_basic_posix){
      principal = new RegexStdBasicPosix();
    }else 
    #endif
    #ifndef NOT_STANDART_EGREP
    if(regex_egrep){
      principal = new RegexStdEgrep();
    }else 
    #endif
    #ifndef NOT_STANDART_GREP
    if(regex_grep){
      principal = new RegexStdGrep();
    }else 
    #endif
    {
      principal = new RegexStdExtendedPosix();
    }
    
    T_STRING input(av[ac-2]);
    output = av[ac-1];
    if(!exclusive_regex){
      principal->internalParseInput(input);
      principal->internalParseOutput(output);
      
      if(print_matched_file != nullptr){
        print_matched_file->print(input,output);
        std::cout << std::endl;
      }
    }
    
    if(!principal->parseRegex(input)){
      principal->printError();
      MY_FREE(principal);
      return -1;
    }
  }
  
  MY_DIR* actual_dir = nullptr;
  MY_DIRENT* ent = nullptr;
  MY_PATH_TYPE f_name;
  T_STRING o_name;
  
  actual_dir = MY_OPENDIR(MY_TO_SYS_TYPE(".").c_str());
  if(actual_dir == nullptr){
    std::cerr << "Unable to read folder." << std::endl;
    MY_FREE(principal);
    MY_FREE(print_matched_file);
    return -1;
  }
  while((ent = MY_READDIR(actual_dir))!=nullptr){
    f_name = ent->d_name;
    if((f_name.compare(MY_DEF_SKIP_1)==0)||(f_name.compare(MY_DEF_SKIP_2)==0))
      continue;
    if(debug)
      print_matched_file->print(f_name,f_name);
    if(principal->match(f_name)){
      principal->replace(f_name,output,o_name);
      if(print_matched_file != nullptr){
        print_matched_file->print(f_name,o_name);
      }
      if(preview_only){
        continue;
      }
      if(MY_RENAME(f_name.c_str(),o_name.c_str()) != 0){
        printRenameError(errno);
      }
    }
  }
  
  MY_FREE(principal);
  MY_FREE(print_matched_file);
  return 0;
}