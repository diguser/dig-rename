PROJECT_NAME=drename
PROJECT_SRC =main.cpp prints.cpp regex_std.cpp wmain.cpp

-include inc.mak

all: $(PJ_EXE_WPATH)

debug executable $(PJ_EXE_WPATH): $(DIRS) $(PJ_OBJ_WPATH)
ifeq ($(L_WIN),0)
	-@echo "$(COLOR_BLUE)Linking$(COLOR_GREEN)    $(PJ_EXE_WPATH)$(COLOR_RED)"
	-@$(LD) -o $(PJ_EXE_WPATH) $(PJ_OBJ_WPATH) $(LDFLAGS_STATIC)
	@if [ -f $(PJ_EXE_WPATH) ]; then echo "$(COLOR_GREEN)           Successfully$(COLOR_DEF)"; else echo "$(COLOR_BLUE)Linking$(COLOR_RED)    ERROR$(COLOR_DEF)"; return 1; fi;
else
	-@echo Linking    $(PJ_EXE_WPATH)
	@$(LD) -o $(PJ_EXE_WPATH) $(PJ_OBJ_WPATH) $(LDFLAGS_STATIC)
	-@echo            Successfully
endif

$(DIRS) $(DIRS_SHARED) $(DIRS_STATIC):
ifeq ($(L_WIN),0)
	-@mkdir $@
else
	-@mkdir $(subst /,\\,$@)
endif

$(DIR_OBJ)/resource.o: $(DIR_RES)/iup.rc
ifeq ($(L_WIN),0)
	-@echo "$(COLOR_BLUE)Resource$(COLOR_GREEN)   $@$(COLOR_RED)"
	-@$(RM) $@
	-@$(WINDRES) $< $@
	@if [ -f $@ ]; then return 0; else echo "$(COLOR_BLUE)Resource$(COLOR_RED)   ERROR$(COLOR_DEF)"; return 1; fi;
else
	-@echo Resource   $@
	@$(WINDRES) $< $@
endif

$(DIR_OBJ)/%.opp: $(DIR_SRC)/%.cpp
ifeq ($(L_WIN),0)
	-@echo "$(COLOR_BLUE)Compile$(COLOR_GREEN)    $@$(COLOR_RED)"
	-@$(RM) $@
ifeq ($(DEBUG_MAKE),1)
	-$(CXX) $(CXXFLAGS_STATIC) -c $< -o $@
else
	-@$(CXX) $(CXXFLAGS_STATIC) -c $< -o $@
endif
	@if [ -f $@ ]; then return 0; else echo "$(COLOR_BLUE)Compile$(COLOR_RED)    ERROR$(COLOR_DEF)"; return 180; fi;
else
	-@echo Compile    $@
	@$(CXX) $(CXXFLAGS_STATIC) -c $< -o $@
endif

$(DIR_OBJ)/%.o: $(DIR_SRC)/%.c
ifeq ($(L_WIN),0)
	-@echo "$(COLOR_BLUE)Compile$(COLOR_GREEN)    $@$(COLOR_RED)"
	-@$(RM) $@
	-@$(CC) $(CCFLAGS_STATIC) -c $< -o $@
	@if [ -f $@ ]; then return 0; else echo "Compile    ERROR\033[0m"; return 1; fi;
else
	-@echo Compile    $@
	@$(CC) $(CCFLAGS_STATIC) -c $< -o $@
endif

color:
ifeq ($(L_WIN),0)
	-@echo "$(COLOR_DEF)"
else
	-@echo.
endif

clean:
ifeq ($(L_WIN),0)
	-@echo "$(COLOR_BLUE)Cleaning project$(COLOR_RED)"
	-$(RM) $(DIR_BIN)
	-$(RM) $(DIR_OBJ)
	-@echo "$(COLOR_DEF)"
else
	-@echo Cleaning project
	-@$(RM) $(subst /,\\,$(DIR_OBJ))
	-@$(RM) $(subst /,\\,$(DIR_BIN))
	-@rmdir /s /q $(DIR_OBJ)
	-@rmdir /s /q $(DIR_BIN)
endif

help:
	@echo         =========== DIG Rename ===========
	@echo.
	@echo Make:
	@echo   all     Compile project (default)
	@echo   clean   Delete all generated files
	@echo   help    Show this help
