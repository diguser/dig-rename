DIG-Rename
==========
Simple program to rename multiple files through a pattern.

Support regex in Extended POSIX grammar and EGREP POSIX grammar.
* Basic POSIX and GREP POSIX can be enabled in [config.hpp](hdr/config.hpp) *

Actually, all regex are processed by `std::regex`. (Working on support for PCRE2)

------------------------------------

Usage
-----
`drename [OPTIONS] pattern-match pattern-replace`

Patterns can be use the system notation for multiples files. (aka. using \* and/or ?)

Sometimes will be need to escape this special characters using \ or |. Anyway, | always will work.

Regex can be used too, but is recommended to add option `-R` or `--regex-only` to skip the filter who convert the system syntax to regex.

Example
-------
`drename "|*.old" "|*.new"`

`drename "20170115|*.jpg" "travel-|*.jpg"`

`drename -R "page-([0-9]+).png" "$1.png"`

License
-------
This program use two libraries and both licenses are inside license folder.

This program is licensed under [GPLv3](http://www.gnu.org/licenses/gpl-3.0.html).