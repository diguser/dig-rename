#ifndef _REGEX_STD_HPP_
#define _REGEX_STD_HPP_ 1

#include "regex.hpp"
#include <regex>

#ifdef UNICODE_SUPPORT
typedef std::wregex iRegex;
#else
typedef std::regex iRegex;
#endif

typedef struct _RegexStd : Regex {
  iRegex regex;
  std::regex_error error;
  bool hError = false;
  
  _RegexStd();
  ~_RegexStd();
  bool match(T_STRING&);
  bool replace(T_STRING&,T_STRING&,T_STRING&);
  bool hasError();
  void printError();
  virtual bool parseRegex(T_STRING&)=0;
} RegexStd;

#ifndef NOT_STANDART_BASIC_POSIX
typedef struct _RegexStdBasicPosix : RegexStd {
  bool parseRegex(T_STRING&);
} RegexStdBasicPosix;
#endif

typedef struct _RegexStdExtendedPosix : RegexStd {
  bool parseRegex(T_STRING&);
} RegexStdExtendedPosix;

#ifndef NOT_STANDART_EGREP
typedef struct _RegexStdEgrep : RegexStd {
  bool parseRegex(T_STRING&);
} RegexStdEgrep;
#endif

#ifndef NOT_STANDART_GREP
typedef struct _RegexStdGrep : RegexStd {
  bool parseRegex(T_STRING&);
} RegexStdGrep;
#endif

#endif