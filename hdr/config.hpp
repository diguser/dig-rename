#ifndef _CONFIG_HPP_
#define _CONFIG_HPP_ 1

/*
  Disables all support for Unicode on Windows platform. 
  It includes support for all non ASCII characters.
*/
//#define NOT_UNICODE

/*
  Disables support for Basic Posix grammar on std::regex
*/
#define NOT_STANDART_BASIC_POSIX

/*
  Disables support for Grep Posix grammar on std::regex
*/
#define NOT_STANDART_GREP

/*
  Disables support for Egrep Posix grammar on std::regex
*/
//#define NOT_STANDART_EGREP

/*
  Disables support for PCRE2 as regex
  :: Still unimplemented.
*/
#define NOT_PCRE2

#endif