#ifndef _UNICODE_HPP_
#define _UNICODE_HPP_ 1

#include "config.hpp"

#if defined(__WIN32__) && !defined(NOT_NOWIDE) && !defined(NOT_UNICODE)
  #include <cwchar>
  #include <nowide/convert.hpp>
  
  #define MY_DIR _WDIR
  #define MY_DIRENT struct _wdirent
  #define MY_OPENDIR(a) _wopendir(a)
  #define MY_READDIR(a) _wreaddir(a)
  #define MY_CLOSEDIR(a) _wclosedir(a)
  #define MY_PATH_TYPE std::wstring
  #define MY_TO_SYS_TYPE(a) nowide::widen(a)
  #define SYS_TO_MY_TYPE(a) nowide::narrow(a)
  #define MY_DEF_SKIP_1 L"."
  #define MY_DEF_SKIP_2 L".."
  #define MY_RENAME(a,b) _wrename(a,b)
  
  #define UNICODE_SUPPORT 1
  #define I_WINDOWS 1
  #define F_MAIN wmain
  #define F_TO_STRING std::to_wstring
  #define C_CONST__(a) L##a
  #define C_CONST(a) C_CONST__(a)
  #define T_REGEX std::wregex
  #define T_STRING std::wstring
  #define T_CHAR wchar_t
  #define T_COUT std::wcout
  #define T_ENDL std::wendl
  
  
#else
  #define MY_DIR DIR
  #define MY_DIRENT struct dirent
  #define MY_OPENDIR(a) opendir(a)
  #define MY_READDIR(a) readdir(a)
  #define MY_CLOSEDIR(a) closedir(a)
  #define MY_PATH_TYPE std::string
  #define MY_TO_SYS_TYPE(a) std::string(a)
  #define SYS_TO_MY_TYPE(a) std::string(a)
  #define MY_DEF_SKIP_1 "."
  #define MY_DEF_SKIP_2 ".."
  #define MY_RENAME(a,b) rename(a,b)
  
  #define F_MAIN main
  #define T_REGEX std::regex
  #define T_STRING std::string
  #define T_CHAR char
#endif

#endif