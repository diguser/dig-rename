#ifndef _PRINTS_HPP_
#define _PRINTS_HPP_ 1

#include "unicode.hpp"
#include <string>

typedef struct PrintMatchedFile {
  virtual ~PrintMatchedFile(){};
  virtual void print(T_STRING&,T_STRING&)=0;
} PrintMatchedFile;

typedef struct:PrintMatchedFile {
  void print(T_STRING&,T_STRING&);
} PrintMatchedOnly;

typedef struct:PrintMatchedFile {
  void print(T_STRING&,T_STRING&);
} PrintResultOnly;

typedef struct:PrintMatchedFile {
  void print(T_STRING&,T_STRING&);
} PrintMatchAndResult;

void printRenameError(const int);
void printHeader();
void printUsage(const T_CHAR*);
void printVersion();
void printManual(const T_CHAR*);

#endif