#ifndef COMPILER_NAME

#ifdef __clang__
  #define COMPILER_NAME __VERSION__
#elif defined __ICC
  #define COMPILER_NAME __VERSION__
#elif defined __GNUG__
  #define COMPILER_NAME "GNU GCC " << __VERSION__
#elif defined __HP_aCC
  #define COMPILER_NAME "HP Compiler " << __HP_aCC
#elif defined __IBMCPP__
  #define COMPILER_NAME "IBM XL C++ " << __xlc__
#elif defined _MSC_VER
  #define COMPILER_NAME "Micro$oft Visual Studio " << _MSC_VER
#elif defined __SUNPRO_CC
  #define COMPILER_NAME "Oracle Solaris Studio " << __SUN_PRO_CC
#elif defined __PGIC__
  #define COMPILER_NAME "Portland Group " << __PGIC__ << "." << __PGIC_MINOR__ << "." << __PGIC_PATCHLEVEL__
#else
  #define COMPILER_NAME "Unknown compiler"
#endif

#if (defined(__i386)||defined(__i386__)||defined(_M_IX86))
  #define PLATFORM "32 bits"
#else
  #define PLATFORM "64 bits"
#endif

#endif