#ifndef _REGEX_HPP_
#define _REGEX_HPP_ 1

#include "unicode.hpp"

typedef struct _Regex {
  virtual ~_Regex(){};
  virtual bool match(T_STRING&)=0;
  virtual bool replace(T_STRING&,T_STRING&,T_STRING&)=0; // input, output-pattern, output
  virtual bool hasError()=0;
  virtual void printError()=0;
  virtual bool parseRegex(T_STRING&)=0;
  virtual void internalParseInput(T_STRING&);
  virtual void internalParseOutput(T_STRING&);
} Regex;

#endif